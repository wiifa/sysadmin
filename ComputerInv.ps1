#* FileName: ComputerInv.ps1
#*=============================================
#* Script Name: [Computer Inventory]
#* Created: [09/15/2007]
#* Author: Jesse Hamrick
#* Company: PowerShell Pro!
#* Email:
#* Web: http://www.powershellpro.com
#* Reqrmnts:
#* Keywords:
#*=============================================
#* Purpose: Computer Invetory of CPU and Disk Properties
#*
#*
#*=============================================
#*=============================================
#* REVISION HISTORY
#*=============================================
#* Date: [DATE_MDY]
#* Time: [TIME]
#* Issue:
#* Solution:
#*
#*=============================================
#*=============================================
#* FUNCTION LISTINGS
#*=============================================
# Function: ListProcessor
# Created: [09/17/2007]
# Author: Jesse Hamrick
# Arguments:
# =============================================
# Purpose: Provides Processor information
#
#
# =============================================
function ListProcessor {
$colItems = get-wmiobject -class �Win32_Processor� -namespace �root\CIMV2? `
-computername $strComputer
foreach ($objItem in $colItems) {
write-host �Caption: � $objItem.Caption
write-host �CPU Status: � $objItem.CpuStatus
write-host �Current Clock Speed: � $objItem.CurrentClockSpeed
write-host �Device ID: � $objItem.DeviceID
write-host �L2 Cache Size: � $objItem.L2CacheSize
write-host �L2 Cache Speed: � $objItem.L2CacheSpeed
write-host �Name: � $objItem.Name
write-host
}
}
#*=============================================
#* Function: ListDisk
#* Created: [09/15/2007]
#* Author: Jesse Hamrick
#* Arguments:
#*=============================================
#* Purpose: Provides Disk Information
#*
#*
#*=============================================
Function ListDisk {
$colItems = get-wmiobject -class �Win32_DiskDrive� -namespace �root\CIMV2? `
-computername $strComputer
foreach ($objItem in $colItems) {
write-host �Description: � $objItem.Description
write-host �Device ID: � $objItem.DeviceID
write-host �Interface Type: � $objItem.InterfaceType
write-host �Media Type: � $objItem.MediaType
write-host �Model: � $objItem.Model
write-host �Partitions: � $objItem.Partitions
write-host �Size: � $objItem.Size
write-host �Status: � $objItem.Status
write-host
}
}
#*==============================================
#* SCRIPT BODY
#*==============================================
# Create a string variable using the local computer.
$strComputer = �.�
# Call the �ListProcessor� function.
ListProcessor
# Call the �ListDisk� function.
ListDisk
#*==============================================
#* END OF SCRIPT: [Computer Inventory]
#*==============================================