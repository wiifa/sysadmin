#* FileName: ComputerInv_v1.1.ps1
#*================================================
#* Script Name: [Computer Inventory]
#* Created: [09/15/2007]
#* Author: Jesse Hamrick
#* Company: PowerShell Pro!
#* Email:
#* Web: http://www.powershellpro.com
#* Reqrmnts:
#* Keywords:
#*================================================
#* Purpose: Computer Invetory of CPU and Disk Properties
#*
#*
#*================================================
#*================================================
#* REVISION HISTORY
#*================================================
#* Date: [10/09/2007]
#* Time: [9:30 AM]
#* Issue: Dot-Source Example
#* Solution:
#*
#*================================================
#*================================================
#* SCRIPT BODY
#*================================================
# Create a string variable using the local computer.
$strComputer = “.”
# Use Dot-Source to Call the “ListProcessor” function.
.{.\CPU.ps1}
# Use Dot-Source to Call the “ListDisk” function.
.{.\Disk.ps1}
#*================================================
#* END OF SCRIPT: [Computer Inventory]
#*================================================